import React, {useContext, useEffect, useRef, useState} from 'react';
import "../styles/admin_panel_style.css";
import Product from "../components/Product";
import FormAddProduct from "../components/FormAddProduct";
import {FormAddProductDisplay, FormInformationUsersDisplay} from "../contacts/ContextDisplayComponents";
import axios from "axios";
import {URL_PRODUCT_ALL, URL_SEARCH_PRODUCT} from "../resources/Url";
import FormInformationUsers from "../components/FormInformationUsers";

const AdminPanel = () => {
    const {displayFormAddProduct, setDisplayFormAddProduct} = useContext(FormAddProductDisplay);
    const {displayFormInformationUsers, setDisplayFormInformationUsers} = useContext(FormInformationUsersDisplay);
    const [listProduct, setListProduct] = useState([]);
    const config = {headers: {'Authorization': sessionStorage.getItem("token").toString()}};
    const searchProductRef = useRef();

    const openFormAddProduct = () => {
        setDisplayFormAddProduct(true);
    }

    const openFormInformationUsers = () => {
        setDisplayFormInformationUsers(true);
    }

    const searchProduct = () => {
        if (searchProductRef.current.value === "") {
            updateListProduct();
        } else {
            axios.get(URL_SEARCH_PRODUCT + searchProductRef.current.value, config).then(rs => {
                if (rs.status === 200) {
                    const products = rs.data;
                    products.sort((a, b) => b.id - a.id);
                    setListProduct(products);
                }
            }).catch(error => {
            });
        }
    }


    const updateListProduct = () => {
        axios.get(URL_PRODUCT_ALL, config).then(rs => {
            const products = rs.data;
            products.sort((a, b) => b.id - a.id);
            setListProduct(products);
        }).catch(error => {
        });
    }

    useEffect(() => {
        updateListProduct();
    }, []);


    return (
        <div id="admin_panel">
            {displayFormAddProduct ? <FormAddProduct updateListProduct={updateListProduct}/> : null}
            {displayFormInformationUsers ? <FormInformationUsers/> : null}
            <header className="header">
                <h3>Telegram_bot_menu</h3>
                <button onClick={openFormAddProduct} className="add_product">Добавить товар</button>
                <input ref={searchProductRef} onChange={searchProduct} className="search_product"
                       placeholder="Поиск товара"/>
                <button onClick={openFormInformationUsers} className="information_users">Информация пользователям</button>
            </header>


            <div className="product_container">
                {listProduct.map(product => {
                    return <Product key={product.id} product={product} updateListProduct={updateListProduct}/>
                })}
            </div>

        </div>
    );
}

export default AdminPanel;