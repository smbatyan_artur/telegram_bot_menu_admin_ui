import React, {useContext, useRef, useState} from 'react';
import "../styles/authentication_page_style.css";
import {URL_SIGN_IN} from "../resources/Url";
import axios from "axios";
import {AdminPanelDisplay, AuthenticationPageDisplay} from "../contacts/ContextDisplayComponents";


const AuthenticationPage = (props) => {
    const {displayAuthenticationPage, setDisplayAuthenticationPage} = useContext(AuthenticationPageDisplay);
    const {displayAdminPanel, setDisplayAdminPanel} = useContext(AdminPanelDisplay);
    let loginRef = useRef();
    let passwordRef = useRef();


    const authenticate = () => {
        const signInForm = {
            "username": loginRef.current.value,
            "password": passwordRef.current.value,
        }


        axios.post(URL_SIGN_IN, signInForm, {}).then(rs => {
            if (rs.status === 200) {
                sessionStorage.setItem('token', rs.data.type + " " + rs.data.token);
                setDisplayAuthenticationPage(false);
                setDisplayAdminPanel(true);
            }
        }).catch(error =>{});
    }


    return (
        <div id="authentication_page">
            <div className="form">
                <input ref={loginRef} placeholder="Логин"/>
                <input ref={passwordRef} type="password" placeholder="Пароль"/>
                <button onClick={authenticate}>Войти</button>
            </div>
        </div>
    );
}

export default AuthenticationPage;