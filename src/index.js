import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import "./styles/zeroing_out.css";
import "./styles/fonts.css";
import "./styles/index.css";


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(<App/>);