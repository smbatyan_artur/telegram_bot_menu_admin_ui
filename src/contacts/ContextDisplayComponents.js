import {createContext} from "react";

export const AuthenticationPageDisplay = createContext(null);
export const AdminPanelDisplay = createContext(null);
export const FormAddProductDisplay = createContext(null);
export const FormInformationUsersDisplay = createContext(null);