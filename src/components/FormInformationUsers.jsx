import React, {useContext, useEffect, useRef, useState} from 'react';
import {FormInformationUsersDisplay} from "../contacts/ContextDisplayComponents";
import axios from "axios";
import {URL_GET_QUANTITY_CHATS, URL_MAILING_CHATS, URL_UPDATE_USER_INFORMATION} from "../resources/Url";

const FormInformationUsers = (props) => {
    const {displayFormInformationUsers, setDisplayFormInformationUsers} = useContext(FormInformationUsersDisplay);
    const [quantityChats, setQuantityChats] = useState(null);
    const greetingRef = useRef();
    const contactsRef = useRef();
    const mailingRef = useRef();
    const config = {headers: {'Authorization': sessionStorage.getItem("token").toString()}};


    useEffect(() => {
        axios.get(URL_GET_QUANTITY_CHATS, config).then(rs => {
            setQuantityChats(rs.data)
        }).catch(error => {
        });
    }, []);

    const closeThisComponent = () => {
        setDisplayFormInformationUsers(false);
    }


    const sendANewsletter = () => {
        if (mailingRef.current.value.trim().length > 0) {
            axios.post(URL_MAILING_CHATS + "?message=" + mailingRef.current.value, {}, config).then(rs => {
                if (rs.status === 200) {
                    alert("Рассылка сообщения выполняется.");
                }
            }).catch(error => {
                alert("Ошибка при рассылке сообщений!")
            });
        } else {
            alert("Рассылка не должна быть пустой!");
        }
    }


    const updateGreeting = () => {
        if (greetingRef.current.value.trim().length > 0) {
            axios.post(URL_UPDATE_USER_INFORMATION + "?text=" + greetingRef.current.value.toString() + "&typeInfo=GREETING", {}, config).then(rs => {
                if (rs.status === 200) {
                    alert("Приветсвие сохранено.");
                }
            }).catch(error => {
                alert("Ошибка при сохранении приветсвия.");
            });
        } else {
            alert("Приветсвие не должно быть пустым!");
        }

    }


    const updateContacts = () => {
        if (contactsRef.current.value.trim().length > 0) {
            axios.post(URL_UPDATE_USER_INFORMATION + "?text=" + contactsRef.current.value.toString() + "&typeInfo=CONTACT_DETAILS", {}, config).then(rs => {
                if (rs.status === 200) {
                    alert("Контакты сохранены.");
                }
            }).catch(error => {
                alert("Ошибка при сохранении контактов.");
            });
        } else {
            alert("Контакты не должны быть пустыми!");
        }
    }

    return (
        <div className="form_information_users">
            <button onClick={closeThisComponent} className="exit">X</button>
            <span className="sum_chats_bot">Количество чатов с ботом: {quantityChats}</span>
            <span className="greeting">Приветствие: <textarea ref={greetingRef} placeholder="Текст"></textarea></span>
            <button onClick={updateGreeting} className="create_greeting">Сохранить приветсвие</button>
            <span className="contacts">Контакты: <textarea ref={contactsRef} placeholder="Текст"></textarea></span>
            <button onClick={updateContacts} className="create_contacts">Созранить контакты</button>
            <span className="mailing">Рассылка: <textarea ref={mailingRef} placeholder="Сообщение"></textarea></span>
            <button onClick={sendANewsletter} className="send">Отправить</button>
        </div>
    );
}

export default FormInformationUsers;