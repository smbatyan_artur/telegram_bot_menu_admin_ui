import React, {useContext, useRef, useState} from 'react';
import {FormAddProductDisplay} from "../contacts/ContextDisplayComponents";
import axios, {post} from "axios";
import {URL_PRODUCT_CREATE} from "../resources/Url";

const FormAddProduct = (props) => {
    const {displayFormAddProduct, setDisplayFormAddProduct} = useContext(FormAddProductDisplay);
    const titleRef = useRef();
    const typeRef = useRef();
    const priceRef = useRef();
    const descriptionRef = useRef();
    const photoRef = useRef();

    const fileChangedHandler = (event) => {
        if (event.target.files[0].size > 1000000) {
            alert("Файл слишком большой! Максимальный размер файла должен состовлять до 1 мб");
            event.target.value = null;
        }
    }

    const closeThisComponent = () => {
        setDisplayFormAddProduct(false);
    }

    const addProduct = () => {
        const config = {
            headers: {
                "authorization": sessionStorage.getItem("token").toString(),
                "Content-Type": "multipart/form-data"
            }
        }
        const creatProductForm = new FormData();

        if (validFormAddProduct()) {
            creatProductForm.append("photo", photoRef.current.files[0]);
            creatProductForm.append("title", titleRef.current.value);
            creatProductForm.append("type", typeRef.current.value);
            creatProductForm.append("price", priceRef.current.value);
            if (descriptionRef.current.value !== "") {
                creatProductForm.append("description", descriptionRef.current.value);
            }

            axios.post(URL_PRODUCT_CREATE, creatProductForm, config).then(rs => {
                if (rs.status === 200) {
                    alert("Продукт добавлен!")
                    titleRef.current.value = null;
                    priceRef.current.value = null;
                    descriptionRef.current.value = null;
                    photoRef.current.value = null;
                    props.updateListProduct();
                }
            }).catch(error => {
                alert("Произошла ошибка при добавлении продукта!")
            });
        }
    }


    const validFormAddProduct = () => {
        if (titleRef.current.value.trim().length > 0
            && typeRef.current.value.trim().length > 0
            && priceRef.current.value.trim().length > 0
            && photoRef.current.value !== null) {
            return true;
        } else {
            alert("Форма не заполнена! Допускаеться только пустое описание.")
        }
    }


    return (
        <div className="form_add_product">
            <button onClick={closeThisComponent} className="exit">X</button>
            <span className="title">Название: <input ref={titleRef} type="text" placeholder="Тексе"/></span>
            <span className="type">Категория: <input ref={typeRef} maxLength="25" type="text"
                                                     placeholder="Текст"/></span>
            <span className="price">Цена: <input ref={priceRef} type="number" placeholder="0" min="0"/> ₽</span>
            <span className="photo">Фото: <input ref={photoRef} onChange={fileChangedHandler} type="file"
                                                 accept="image/png, image/jpeg"/></span>
            <span className="description">Описание: <textarea ref={descriptionRef}
                                                              placeholder="Текст"></textarea></span>
            <button onClick={addProduct} className="add">Добавить</button>
        </div>
    );
}

export default FormAddProduct;