import React, {useRef} from 'react';
import axios from "axios";
import {URL_CHANGE_PRODUCT_PRICE, URL_DELETE_PRODUCT} from "../resources/Url";


const Product = (props) => {
    const priceRef = useRef();
    const config = {headers: {'Authorization': sessionStorage.getItem("token").toString()}};


    const deleteProduct = () => {
        axios.delete(URL_DELETE_PRODUCT + props.product.id, config).then(rs => {
            if (rs.status === 200) {
                alert("Удален.")
                props.updateListProduct();
            }
        }).catch(erorr => {
            alert("При удалении произошла ошибка.")
        });
    }


    const changeProductPrice = () => {
        if (priceRef.current.value !== "" && priceRef.current.value !== null) {
            axios.patch(URL_CHANGE_PRODUCT_PRICE + "?product_id=" + props.product.id + "&price=" + priceRef.current.value, {}, config).then(rs => {
                if (rs.status === 200) {
                    alert("Цена изменена.");
                }
            }).catch(error => {
                alert("При изменении цены произошла ошибка.")
            });
        }
    }

    return (
        <div className="product">
            <span className="title">Название: {props.product.title}</span>
            <span className="price">Цена: <input ref={priceRef} placeholder={props.product.price} type="number"
                                                 min="0"/> ₽</span>
            <button onClick={changeProductPrice} className="change">Изменить цену</button>
            <span className="type">Категория: {props.product.type}</span>
            <div className="description">Описание: {props.product.description}</div>
            <button onClick={deleteProduct} className="delete">Удалить</button>
        </div>
    );
}

export default Product;