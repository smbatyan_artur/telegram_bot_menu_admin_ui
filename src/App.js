import React, {useEffect, useState} from 'react';
import {
    AdminPanelDisplay,
    AuthenticationPageDisplay,
    FormAddProductDisplay,
    FormInformationUsersDisplay
} from "./contacts/ContextDisplayComponents";
import AuthenticationPage from "./pages/AuthenticationPage";
import AdminPanel from "./pages/AdminPanel";
import FormInformationUsers from "./components/FormInformationUsers";


function App() {
    const [displayAuthenticationPage, setDisplayAuthenticationPage] = useState(null);
    const [displayAdminPanel, setDisplayAdminPanel] = useState(null);
    const [displayFormAddProduct, setDisplayFormAddProduct] = useState(false);
    const [displayFormInformationUsers, setDisplayFormInformationUsers] = useState(false);

    useEffect(() => {
        if (sessionStorage.getItem("token") === null) {
            setDisplayAuthenticationPage(true);
            setDisplayAdminPanel(false);
        } else {
            setDisplayAuthenticationPage(false);
            setDisplayAdminPanel(true);
        }
    }, []);


    return (
        <FormInformationUsersDisplay.Provider value={{displayFormInformationUsers, setDisplayFormInformationUsers}}>
            <FormAddProductDisplay.Provider value={{displayFormAddProduct, setDisplayFormAddProduct}}>
                <AuthenticationPageDisplay.Provider value={{displayAuthenticationPage, setDisplayAuthenticationPage}}>
                    <AdminPanelDisplay.Provider value={{displayAdminPanel, setDisplayAdminPanel}}>
                        <div className="App">
                            {displayAuthenticationPage ? <AuthenticationPage/> : null}
                            {displayAdminPanel ? <AdminPanel/> : null}
                        </div>
                    </AdminPanelDisplay.Provider>
                </AuthenticationPageDisplay.Provider>
            </FormAddProductDisplay.Provider>
        </FormInformationUsersDisplay.Provider>
    );
}

export default App;
